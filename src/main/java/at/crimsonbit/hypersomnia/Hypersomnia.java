package at.crimsonbit.hypersomnia;

import at.crimsonbit.hypersomnia.common.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.SidedProxy;

@Mod(modid = Hypersomnia.MODID, name = Hypersomnia.name, version = "1.0")
public class Hypersomnia {

	public static final String MODID = "hypersomnia";
	public static final String name = "Hypersomnia";

	@Instance
	public static Hypersomnia instance;

	@SidedProxy(clientSide = "at.crimsonbit.hypersomnia.client.ClientProxy", serverSide = "at.crimsonbit.hypersomnia.server.ServerProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit(e);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}

}
