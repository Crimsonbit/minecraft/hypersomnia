package at.crimsonbit.hypersomnia.common.eventhandler;

import at.crimsonbit.hypersomnia.common.blocks.HypersomniaBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.event.entity.player.SleepingLocationCheckEvent;
import net.minecraftforge.event.entity.player.SleepingTimeCheckEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;

public class SleepTimeHandler {
	@SubscribeEvent
	public void onSleepTimeCheckEvent(SleepingTimeCheckEvent e) {
		if (e.getSleepingLocation() == null) {
			e.setResult(Result.ALLOW);
			return;
		}
		IBlockState bs = e.getEntity().world.getBlockState(e.getSleepingLocation());
		if (bs.getBlock() == HypersomniaBlocks.dayBed)
			e.setResult(e.getEntity().world.isDaytime() ? Result.ALLOW : Result.DENY);
	}

	@SubscribeEvent
	public void onSleepLocationCheckEvent(SleepingLocationCheckEvent e) {
		if (e.getSleepingLocation() == null) {
			e.setResult(Result.ALLOW);
			return;
		}
		IBlockState bs = e.getEntity().world.getBlockState(e.getSleepingLocation());
		if (bs.getBlock() == HypersomniaBlocks.dayBed)
			e.setResult(e.getEntity().world.isDaytime() ? Result.ALLOW : Result.DENY);
	}

	@SubscribeEvent
	public void onPlayerWakeupEvent(PlayerWakeUpEvent e) {
		EntityPlayer player = e.getEntityPlayer();
		if (!player.isPlayerFullyAsleep())
			return;
		if (player.world.isRemote) {
			System.out.println("ClientTime: " + player.world.getWorldTime());
			return;
		}
		IBlockState bs = player.world.getBlockState(player.bedLocation);
		if (bs.getBlock() == HypersomniaBlocks.dayBed) {
			System.out.println("ServerTime: " + player.world.getWorldTime());
			long i = player.world.getWorldTime();
			player.world.setWorldTime((i - i % 24000L) + 13000);
		}
	}
}
