package at.crimsonbit.hypersomnia.common.eventhandler;

import at.crimsonbit.hypersomnia.Hypersomnia;
import at.crimsonbit.hypersomnia.common.blocks.BlockDayBed;
import at.crimsonbit.hypersomnia.common.items.ItemDayBed;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RegistryHandler {
	@SubscribeEvent
	public void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().register(new BlockDayBed("daybed"));
	}

	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> event) {
		Item daybed = new ItemDayBed("daybed");
		event.getRegistry().register(daybed);
		Hypersomnia.proxy.registerTexture(daybed, 0, new ModelResourceLocation(daybed.getRegistryName(), "inventory"));
	}
}
