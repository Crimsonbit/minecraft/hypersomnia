package at.crimsonbit.hypersomnia.common.blocks;

import javax.annotation.Nullable;

import at.crimsonbit.hypersomnia.common.items.HypersomniaItems;
import net.minecraft.block.BlockBed;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider.WorldSleepResult;

public class BlockDayBed extends BlockBed {

	public BlockDayBed(String name) {
		super();
		this.setRegistryName(name);
		this.setUnlocalizedName(name);
		this.hasTileEntity = false;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
			EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (worldIn.isRemote)
			return true;

		WorldSleepResult wsr = worldIn.provider.canSleepAt(playerIn, pos);
		if (wsr == WorldSleepResult.DENY)
			return true;

		if (wsr == WorldSleepResult.BED_EXPLODES) {
			worldIn.setBlockToAir(pos);
			BlockPos blockpos = pos.offset(((EnumFacing) state.getValue(FACING)).getOpposite());

			if (worldIn.getBlockState(blockpos).getBlock() == this) {
				worldIn.setBlockToAir(blockpos);
			}

			worldIn.newExplosion((Entity) null, (double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D,
					(double) pos.getZ() + 0.5D, 10.0F, true, true);
			return true;
		}

		if (wsr == net.minecraft.world.WorldProvider.WorldSleepResult.DENY)
			return true;
		
		if (((Boolean) state.getValue(OCCUPIED)).booleanValue()) {
			EntityPlayer entityplayer = _getPlayerInBed(worldIn, pos);

			if (entityplayer != null) {
				playerIn.sendStatusMessage(new TextComponentTranslation("tile.bed.occupied", new Object[0]), true);
				return true;
			}

			state = state.withProperty(OCCUPIED, Boolean.valueOf(false));
			worldIn.setBlockState(pos, state, 4);
		}

		// EntityPlayer.SleepResult entityplayer$sleepresult = _trySleep(pos, playerIn,
		// worldIn);
		EntityPlayer.SleepResult entityplayer$sleepresult = playerIn.trySleep(pos);

		if (entityplayer$sleepresult == EntityPlayer.SleepResult.OK) {
			state = state.withProperty(OCCUPIED, Boolean.valueOf(true));
			worldIn.setBlockState(pos, state, 4);
			return true;
		} else {

			if (entityplayer$sleepresult == EntityPlayer.SleepResult.NOT_POSSIBLE_NOW) {
				playerIn.sendStatusMessage(new TextComponentString("You can only sleep at daytime!"), true);
			} else if (entityplayer$sleepresult == EntityPlayer.SleepResult.NOT_SAFE) {
				playerIn.sendStatusMessage(new TextComponentTranslation("tile.bed.notSafe", new Object[0]), true);
			} else if (entityplayer$sleepresult == EntityPlayer.SleepResult.TOO_FAR_AWAY) {
				playerIn.sendStatusMessage(new TextComponentTranslation("tile.bed.tooFarAway", new Object[0]), true);
			}

			return true;
		}
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state) {
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public MapColor getMapColor(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
		return this.blockMapColor;
	}

	@Override
	public void onLanded(World worldIn, Entity entityIn) {
		if (entityIn.isSneaking()) {
			super.onLanded(worldIn, entityIn);
		} else if (entityIn.motionY < 0.0D) {
			entityIn.motionY = -entityIn.motionY * 0.75D;

			if (!(entityIn instanceof EntityLivingBase)) {
				entityIn.motionY *= 0.8D;
			}
		}
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return null;
	}
	
	@Override
	public boolean hasTileEntity() {
		return false;
	}

	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
		return new ItemStack(HypersomniaItems.dayBed);
	}

	@Override
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos,
			EntityPlayer player) {
		return new ItemStack(HypersomniaItems.dayBed);
	}

	@Nullable
	private EntityPlayer _getPlayerInBed(World worldIn, BlockPos pos) {
		for (EntityPlayer entityplayer : worldIn.playerEntities) {
			if (entityplayer.isPlayerSleeping() && entityplayer.bedLocation.equals(pos)) {
				return entityplayer;
			}
		}

		return null;
	}

}
